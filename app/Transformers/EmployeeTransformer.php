<?php


namespace App\Transformers;

use App\Models\Employee;

class EmployeeTransformer extends BaseTransformer
{
    public function transform($employee): array{
        return [
            'surname' => $employee->surname,
            'firstname' => $employee->firstname,
            'pin' => $employee->pin
        ];
    }
}
