<?php

namespace App\Domain;

use App\Models\User;
use Illuminate\Auth\AuthManager;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;

use Spatie\Fractal\Fractal;
use Spatie\Fractalistic\ArraySerializer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class BaseDomainService
{
    use ValidatesRequests;

    /**
     * Incoming HTTP Request
     *
     * @var Request
     */
    protected $request;

    /**
     * Authentication Service
     *
     * @var AuthManager
     */
    protected $auth;

    /**
     * Fractal service
     *
     * @var Fractal
     */
    protected $fractal;

    /**
     * BaseDomainService constructor.
     */
    public function __construct()
    {
        $this->request = app(Request::class);
        $this->auth = app(AuthManager::class);
        $this->fractal = app(Fractal::class)->serializeWith(new ArraySerializer());
    }

    /**
     * HTTP Request: Getter
     *
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * HTTP Request: Setter
     *
     * @param Request $request
     */
    public function setRequest($request)
    {
        $this->request = $request;
    }

    /**
     * Get Authenticated User
     *
     * @return User|null
     */
    public function getUser()
    {
        return $this->auth->user();
    }

    /**
     * Get Authenticated User or Fail with an Unauthorized Exception
     *
     * @return User|null
     */
    public function getUserOrFail()
    {
        $user = $this->getUser();

        if ($user !== null) {
            return $user;
        }

        throw new HttpException(Response::HTTP_UNAUTHORIZED, __('auth.unauthorized'));
    }
}
