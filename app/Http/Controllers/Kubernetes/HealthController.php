<?php


namespace App\Http\Controllers\Kubernetes;


use App\Http\Controllers\Controller;

class HealthController extends Controller
{
    /**
     * Main entry function
     */
    public function __invoke()
    {
        return 'ok';
    }
}
