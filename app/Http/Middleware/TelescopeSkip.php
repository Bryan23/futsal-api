<?php

namespace App\Http\Middleware;

use Closure;
use Laravel\Telescope\Telescope;

class TelescopeSkip
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (class_exists(Telescope::class)) {
            Telescope::stopRecording();
        }

        return $next($request);
    }
}
