<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

class BaseRepository
{
    /**
     * @var \Illuminate\Database\Eloquent\Builder
     */
    protected $query;

    /**
     * @var Model
     */
    protected $model;

    /**
     * BaseRespository constructor.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }


    /**
     * Get new query builder
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getQuery()
    {
        return $this->model->query();
    }

    /**
     * Get Eloquent model
     *
     * @return Model
     */
    public function getModel()
    {
        return $this->model;
    }

    public function find($id)
    {
        return $this->getQuery()->find($id);
    }
}
