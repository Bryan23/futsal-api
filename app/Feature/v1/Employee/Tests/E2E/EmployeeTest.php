<?php

namespace App\Feature\v1\Employee\Tests\E2E;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class EmployeeTest extends TestCase
{
    use RefreshDatabase, withFaker;

    protected function setUp(): void
    {
        parent::setUp();
        $this->seed();
    }
}
