<?php


namespace App\Feature\v1\Employee\Http\Controllers;


use Illuminate\Http\Request;


class FutsalRamdomizerController
{
    public function __construct()
    {
        //constructor
    }

    public function __invoke(Request $request)
    {
        $playerList = $request->get("Team");
        $specialPlayer = $request->get("Special");

        if ($specialPlayer != null){
            $teams = $this->specialShuffle($specialPlayer, $playerList);
        } else {
            $teams = $this->shuffleTeam($playerList);
        }

        return [
            "Team1" => $teams[0],
            "Team2" => $teams[1]
        ];
    }

    /**
     * @param $playerList
     * @return array
     */
    private function shuffleTeam($playerList){
        shuffle($playerList);

        $totalRating = 0;
        $teamIsNotBalanced = true;
        $noOfPlayer = count($playerList);
        $index = 0;
        $teamRating = 0;

        foreach ($playerList as $player) {
            $totalRating += $player['rating'];
        }

        $teamAvgRating = $totalRating / 2;

        while ($teamIsNotBalanced) {
            for ($i = 0; $i < $noOfPlayer / 2; $i++) {
                $team1[$i] = $playerList[$i];
            }

            for ($j = $noOfPlayer / 2; $j < $noOfPlayer; $j++) {
                $team2[$index] = $playerList[$j];
                $index++;
            }

            foreach ($team1 as $player1) {
                $teamRating += $player1['rating'];
            }

            if ($teamRating >= $teamAvgRating - 2.5 && $teamRating <= $teamAvgRating + 2.5) {
                $teamIsNotBalanced = false;
            } else {
                $teamIsNotBalanced = true;
                shuffle($playerList);
            }
        }

        return array($team1, $team2);
    }

    /**
     * @param $specialPlayer
     * @param $playerList
     * @return array
     */
    private function specialShuffle($specialPlayer, $playerList){
        if ($specialPlayer != null && count($specialPlayer) == 2){
            $specialPlayer1 = $specialPlayer[0];
            $specialPlayer2 = $specialPlayer[1];
        }

        $key1 = array_search($specialPlayer1, $playerList);
        $key2 = array_search($specialPlayer2, $playerList);
        unset($playerList[$key1]);
        unset($playerList[$key2]);

        $teams = $this->shuffleTeam($playerList);
        array_push($teams[0], $specialPlayer1);
        array_push($teams[1], $specialPlayer2);
        shuffle($teams[0]);
        shuffle($teams[1]);

        return $teams;
    }
}
