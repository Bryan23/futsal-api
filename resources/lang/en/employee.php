<?php

return [
    'exceptions' => [
        'not_found' => 'Pin does not exists! Please contact management for further assistance.'
    ],

    'messages' => [
        'welcome' => 'Good Morning!'
    ]
];
