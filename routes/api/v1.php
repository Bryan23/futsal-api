<?php

use App\Feature\v1\Employee\Http\Controllers\FutsalRamdomizerController;
use Illuminate\Support\Facades\Route;

Route::post("randomize/futsal-team", FutsalRamdomizerController::class);
